from update import DISABLE_MAP, UPDATE1, UPDATE2, UPDATE3
from threading import Thread
import os

def run_fake_update(logger=None, is_thread=False):
    if is_thread:
        try:
            os.system("loadkeys {0} && fbi -noverbose -t 1 -a {1} {2} {3}".format(DISABLE_MAP,
                                                                                          UPDATE1, UPDATE2, UPDATE3))
        except Exception as e:
            if logger:
                logger.critical("Error while executing fake update: {0}".format(repr(e)))
    else:
        Thread(target=run_fake_update, args=(logger, True)).start()
