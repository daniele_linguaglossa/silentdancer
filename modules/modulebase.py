from argparse import Namespace
import sys
import os

class Base(object):
    platforms = Namespace(Win2000Serv2000=1,
                          WinXP=2,
                          WinXPProServ2003R2=4,
                          WinVistaServ2008=8,
                          Win7Serv2008R2=16,
                          Win8Serv2012R2=32,
                          Win81Serv2012R2=64,
                          Win10Serv2016=128,
                          Kern25x=256,
                          Kern26x=512,
                          Kern3x=1024,
                          Kern4x=2048,
                          WinAll=255,
                          KernAll=3840,
                          All=4095)
    arch_types = Namespace(All=3, x86=1, x86_64=2)
    platform_label = {
        128: "Windows 10 / Windows Server 2016",
        64: "Windows 8.1 / Windows Server 2012 R2",
        32: "Windows 8 / Windows Server 2012 R2",
        16: "Windows 7 / Windows Server 2008 R2",
        8: "Windows Vista / Windows Server 2008",
        4: "Windows XP Professional / Windows Server 2003 R2",
        2: "Windows XP ",
        1: "Windows 2000 / Windows Server 2000",
        2048: "Linux Kernel 4.x",
        1024: "Linux Kernel 3.x",
        512: "Linux Kernel 2.6.x",
        256: "Linux Kernel 2.5.x"
    }
    arch_label = {
        1: "x86 (32 bit)",
        2: "x86_64 (64 bit)"
    }
    name = "Noname"
    author = "Noname"
    description = ["", ""]
    mail = "Noname"
    arch = 0
    platform = 0

    def set_info(self, module_name="", author="", small_description="", full_description="", mail="", arch=0, platform=0):
        self.name = module_name.upper()
        self.author = author
        self.description[0] = small_description
        self.description[1] = full_description
        self.mail = mail
        self.arch = arch
        self.platform = platform

    def set_sock(self, sock):
        self.sock = sock

    def set_ip(self, ip):
        self.ip = ip

    def set_zombie_name(self, name):
        self.zombie_name = name

    def info(self, text):
        sys.stdout.write("[*] {0}\n".format(text))

    def warning(self, text):
        sys.stdout.write("[!!] {0}\n".format(text))

    def error(self, text):
        sys.stdout.write("[-] {0}\n".format(text))

    def help(self, full=False):
        out = ""
        out += "{0} - {1} - {2}\n\n".format(self.name, self.author, self.mail)
        out += "Description:\n{0}\n\n".format(self.description[1] if full else self.description[0])
        out += "Platforms:\n"
        for platform in self.platform_label:
            if platform & self.platform == platform:
                out += "  {0}\n".format(self.platform_label[platform])
        out += "\nArch:\n"
        for arch in self.arch_label:
            if arch & self.arch == arch:
                out += "  {0}\n".format(self.arch_label[arch])
        out += "\n"
        return out

    def is_windows(self, mount_path):
        if os.path.isdir(mount_path + os.sep + "Boot") and os.path.exists(mount_path + os.sep + "bootmgr"):
            return True
        return False

    def is_linux(self, mount_path):
        if os.path.isdir(mount_path + os.sep + "boot") and os.path.exists(mount_path + os.sep + "etc/passwd"):
            return True
        else:
            return False

    def mount(self, device, mount_path):
        os.system("mount {0} {1}".format(device, mount_path))

    def umount(self, device):
        os.system("umount {0}".format(device))

    def make_mount(self, path):
        if not os.path.isdir(path):
            os.mkdir(path)
        return path if path[-1] != "/" else path[:-1]
