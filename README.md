# SilentDancer #

RedTeam USB pentest tool

### What is? ###

* SilentDancer is a bootable OS linux based that allow remote administration over connected HDD
* Version 0.1 beta

### Setup a custom linux image? ###

In order to setup a custom linux image there are some dependencies:
 - fbi (print raw image to framebuffer)
 - exiftool (get executable information used during OS fingerprint)
 - Python (in order to run silentdancer)
 - Python-virtualenv (create a virtual environment with all dependencies)

### Setup ###

In order to setup do the following
```
sudo su
apt-get install fbi exiftool python-virtualenv
cd ~ 
virtualenv silentdancer
git clone https://username@bitbucket.org/daniele_linguaglossa/silentdancer.git
source /root/silentdancer/bin/activate && pip install -r requirements.txt && deactivate
echo 'source /root/silentdancer/bin/activate' >> ~/.bashrc
echo 'python /root/server.py' >> ~/.bashrc
echo 'Ready to go!'
```

### How it works? ###

When the OS starts silentdancer will start to send UDP boradcast packets, our client will catch those packets and connect back to the target machine.
A fake windows update screen will be launched and keyboard will be disabled in order to prevent shutdown.

### Features ###

 - Modular
 - Multiple connections
 - SSL enabled
 - Stealth!