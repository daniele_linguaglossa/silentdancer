import ConfigParser
import os

INI_PATH = "".join(__path__) + os.sep + "config.ini"


class Config(ConfigParser.ConfigParser):

    config = {}

    def __init__(self):
        ConfigParser.ConfigParser.__init__(self)
        self.read(INI_PATH)
        self.config.update({"use-ssl": self.getboolean("Server", "use-ssl")})
        self.config.update({"fake-update": self.getboolean("Server", "fake-update")})

    def get_config(self):
        return self.config