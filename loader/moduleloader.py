from modules import MODULE_PATH
from os import listdir
from os.path import isfile, join
import sys


def get_defined_modules(logger, info, sock):
    sys.path.append(MODULE_PATH)
    modules = {}
    files = [f for f in listdir(MODULE_PATH) if isfile(join(MODULE_PATH, f)) and f[-3:] == ".py" and "__init__" not in f]
    for module in files:
        try:
            module = __import__(module[:-3])
            if "Module" in module.__dict__:
                module = module.Module()
                module.set_sock(sock)
                module.set_ip(info.split(":")[1])
                module.set_zombie_name(info.split(":")[0])
                modules.update({module.name: module})
        except Exception as e:
            logger.error("Unable to include module {0}: {1}".format(module[:-3], repr(e)))
    return modules
